<?php include_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Activity: Classes, Objects, Inheritance and Polymorphism</title>
    </head>
    <body>
        <h1>Person</h1>
        <p><?php echo $person->printName(); ?></p>

        <h1>Developer</h1>
        <p><?php echo $developer->printName(); ?></p>

        <h1>Engineer</h1>
        <p><?php echo $engineer->printName(); ?></p>

    </body>
</html>